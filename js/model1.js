  function AppViewModel() {
             this.pageTitle = "IBAT on Knockout";
     this.firstName = ko.observable("Bertx");
    this.lastName = ko.observable("Bertingtonx");
    
    this.fullName = ko.computed(function () {
    return this.firstName() + " " + this.lastName()
    },this);
    
     this.capitalizeLastName = function() {
        var currentVal = this.lastName();        // Read the current value
        this.lastName(currentVal.toUpperCase()); // Write back a modified value
    };
    
    
}

